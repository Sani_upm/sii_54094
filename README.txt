
README

------INSTRUCCIONES DE USO-------------------------

	El jugador deberá mover su raqueta correspondiente y evitar que la bola choque contra la pared de detras.
	Si la bola choca con la raqueta, rebotará hacía la pared del otro jugador.
	Si la bola choca con la pared de detras de cada raqueta, se sumará un punto al rival.
	El objetivo del juego es anotar cuantos más puntos sea posible.


	Como elemento adicional, cada jugador tendrá la opcion de disparar al otro jugador, si el disparo impacta contra
	la raqueta del oponente, este no podra moverse hasta que pasen 2 segundos.


MOVIMIENTO JUGADOR 1:

	"w": mueve la raqueta hacia arriba.
	"s": mueve la raqueta hacia abajo.
	"e": disparo.

MOVIMIENTO JUGADOR 2:

	"o": mueve la raqueta hacia arriba.
	"l": mueve la raqueta hacia abajo.
	"p": disparo.
