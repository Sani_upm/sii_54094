
# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.2.0] - 2021-10-28

### Changed
- Añadiendo movimiento a raquetas y pelota
- Change log

## [1.0.0] - 2021-10-06

### Changed
- Cambiando los archivos .cpp
- Cambiando los archivos .h
- Rename folders
- Merged rcedazo/sii into master
- Clase Socket para la practica4
- Codigo inicial de la practica 1

