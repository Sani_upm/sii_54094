// Esfera.h: interface for the Esfera class.
// Autor : Sani Mitkov 54094
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
#define AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Vector2D.h"

class Esfera  
{
public:	
	Esfera();
	virtual ~Esfera();
		
	Vector2D centro;
	Vector2D velocidad;
	float radio;

	void Mueve(float t);
	void setPos(float x, float  y) {

		centro.x = x;
		centro.y = y;
	}
	Vector2D getPos(){ 
		Vector2D pos;
		pos.x = centro.x;
		pos.y = centro.y;
		return pos;
	}
	void setRadio(float r) { radio = r; }
	void setVel(float vx, float vy) {
		velocidad.x = vx;
		velocidad.y = vy;
	}
	void Dibuja();
};

#endif // !defined(AFX_ESFERA_H__8D520BAF_8208_423B_BD91_29F6687FB9C3__INCLUDED_)
