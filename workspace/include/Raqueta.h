// Raqueta.h: interface for the Raqueta class.
// Autor : Sani Mitkov 54094
//////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include "Plano.h"
#include "Vector2D.h"
#include "Esfera.h"

class Raqueta : public Plano  
{
protected:
	int contador = 0;
	bool coolDown = false;
	
public:
	Vector2D velocidad;

	Raqueta();
	virtual ~Raqueta();
	Esfera* Disparo(float vx);

	float getDistancia(Esfera disparo);
	bool Impacto(Esfera disparo);
	void CoolDown(Esfera &disparo);
	Vector2D getPos(){ 

		Vector2D pos;
		pos.x = (x1 + x2) / 2;
		pos.y = (y1 + y2) / 2;
		return pos;
	}

	void Mueve(float t);
};
