// Raqueta.cpp: implementation of the Raqueta class.
// Autor : Sani Mitkov 54094
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	
}

Raqueta::~Raqueta()
{

}

Esfera* Raqueta::Disparo(float vx)
{
	Esfera disparo;
	disparo.centro.y=(y1 + y2) / 2;
	disparo.centro.x = (x1 + x2) / 2;
	disparo.velocidad.x = vx;
	disparo.velocidad.y = 0;
	disparo.radio = 0.25;
	return &disparo;
}

void Raqueta::Mueve(float t)
{
if(coolDown==false){
	y1 += velocidad.y * t;
	y2 += velocidad.y * t;
	x1 += velocidad.x * t;
	x2 += velocidad.x * t;
	contador=0;
	}

}
float Raqueta::getDistancia(Esfera disparo)
{
	return abs(this->getPos().x-disparo.centro.x);	
}

bool Raqueta::Impacto(Esfera disparo){
	bool collisionX;
	if(abs(disparo.centro.x-x1)<=0.1)
	collisionX=true;
	else collisionX=false;
	bool collisionY=(disparo.centro.y<y1)&&(disparo.centro.y>y2);
	//printf("disparo:%f, y1:%f,y2:%f\n",disparo.centro.y,y1,y2);
	/*if(collisionY)
	printf("IMPACTOY\n");
	if(collisionX)
	printf("IMPACTOX\n");*/
	return collisionX && collisionY;
}
void Raqueta::CoolDown(Esfera &disparo){
	if(this->Impacto(disparo)){
			coolDown=true;
			printf("Impacto\n");
	}
	if((coolDown==true)&&(contador<80)){
		contador++;
		printf("Cooldown activado, contador :%d\n",contador);
		disparo.centro.x=5;
		disparo.centro.y=10;
	}else{
	coolDown=false;
	contador=0;
	}
}

